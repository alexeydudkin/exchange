package com.exchange.bl

import akka.actor.{Actor, ActorLogging, Props}
import com.exchange.Messages._
import com.exchange.model.{Client, Order}
import com.exchange.Operation._

object ClientActor {
  def props(client: Client) = Props(classOf[ClientActor], client)
}
/** Client Actor
  * Represents each client and processes client orders
  * @param initClient initial state of the client
  * */
class ClientActor(initClient: Client) extends Actor with ActorLogging {

  override def receive: Receive = processClientOrder(initClient)

  private def processClientOrder(client: Client): Receive = {
    case GetClientInfo =>
      sender() ! ClientInfoResponse(client)

    case msg@ApplyClientOrder(order: Order) =>
      log.debug(s"Receive client order message: $msg for client: $client")
      context become processClientOrder(
        processOrder(client, order)
      )
    case ExportClientInfo(exporter) =>
      exporter ! client
  }

  private def processOrder(client: Client, order: Order): Client =
    order.operation match {
      case Buy =>
        log.debug(s"Process buy operation for ${client.name}")
        client.clientStock get order.stock collect {
          case count if client.balanceUSD - order.quantity * order.price >= 0 =>
            client.copy(
              clientStock = client.clientStock + (order.stock -> (count + order.quantity)),
              balanceUSD = client.balanceUSD - order.quantity * order.price
            )
        } getOrElse client
      case Sell =>
        log.debug(s"Process sell operation for ${client.name}")
        client.clientStock get order.stock collect {
          case count if count >= order.quantity =>
            client.copy(
              clientStock = client.clientStock + (order.stock -> (count - order.quantity)),
              balanceUSD = client.balanceUSD + order.quantity * order.price
            )
        } getOrElse client
    }
}
