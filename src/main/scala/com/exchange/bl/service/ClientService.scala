package com.exchange.bl.service

import akka.actor.{ActorContext, ActorRef}
import com.exchange.bl.ClientActor
import com.exchange.model.Client
import com.exchange.model.Client.ClientName

trait ClientService {
  def storeClientsRef(clients: Set[Client])(context:ActorContext): Map[ClientName, ActorRef]
}

class ClientServiceImpl extends ClientService {
  override def storeClientsRef(clients: Set[Client])(context:ActorContext): Map[ClientName, ActorRef] =
    clients.map { client =>
      client.name -> context.actorOf(ClientActor.props(client))
    } toMap

}
