package com.exchange.bl

import java.nio.file.Paths

import com.typesafe.config.ConfigFactory

trait Cfg {
  val config = ConfigFactory.load()

  val clientsPath = Paths.get(config.getString("importer.clients-path"))
  val ordersPath = Paths.get(config.getString("importer.orders-path"))
  val clientParallelism = config.getInt("importer.client-parallelism")
  val orderParallelism = config.getInt("importer.order-parallelism")
}

object ExchangeCfg extends Cfg
