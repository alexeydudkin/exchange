package com.exchange.bl

import java.nio.file.Paths

import ExchangeCfg._
import akka.NotUsed
import akka.actor.{ActorRef, ActorSystem}
import akka.stream.{ActorMaterializer, IOResult, OverflowStrategy}
import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink, Source}
import akka.util.ByteString
import com.exchange.model.Client
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

/** Exporter
  * Store result to file
  * */
case class Exporter(implicit system: ActorSystem) extends LazyLogging {
  implicit val materializer = ActorMaterializer()
  implicit val execCtx = system.dispatcher

  private val storeClientsPath = Paths.get(config.getString("exporter.store-clients-path"))
  private val clientsBufferSize = config.getInt("exporter.clients-buffer-size")

  /** Store clients to text file
    *
    * @param matFunc function, which transform materialized value
    **/
  def storeClients(matFunc: ActorRef => Unit) =
    Source.actorRef[Client](clientsBufferSize, OverflowStrategy.dropHead)
      .mapMaterializedValue(ref => matFunc(ref))
      .via(exportFlow)
      .map(ByteString.apply)
      .runWith(FileIO.toPath(storeClientsPath))
      .onComplete(_ =>
        system.terminate()
      )

  val exportFlow: Flow[Client, String, NotUsed] =
    Flow[Client]
      .map(client => client.prettyPrint + "\n")

}
