package com.exchange.bl

import akka.NotUsed
import akka.actor.{ActorRef, ActorSystem}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{FileIO, Flow, Framing, Sink}
import akka.util.ByteString
import ExchangeCfg._
import com.exchange.Messages._
import com.exchange._
import com.exchange.model.{Client, Order}
import com.typesafe.scalalogging.LazyLogging
import scala.collection.immutable.ListMap
import scala.concurrent.Future
import scala.util.{Failure, Try}
import scala.util.control.NonFatal

/** Importer
  * Import clients and orders from files
  *
  * @param clientManager client manager reference
  * */
case class Importer(clientManager: ActorRef)(implicit system: ActorSystem) extends LazyLogging {
  implicit val materializer = ActorMaterializer()
  implicit val execCtx = system.dispatcher


  def processClients = FileIO.fromPath(clientsPath)
    .via(fileProcessFlow(clientParallelism)(parseClientLine))
    .collect { case Some(client) => ProcessClient(client) }
    .runWith(Sink.actorRef(clientManager, ProcessClientsComplete))

  def processOrders = FileIO.fromPath(ordersPath)
    .via(fileProcessFlow(orderParallelism)(parseOrderLine))
    .collect { case Some(order) => ApplyClientOrder(order) }
    .runWith(Sink.actorRef(clientManager, ProcessOrdersComplete))

  def fileProcessFlow[T](parallelism: Int)
    (lineParser: String => Future[T]): Flow[ByteString, T, NotUsed] =
    Flow[ByteString]
      .via(lineDelimiter)
      .map(_.utf8String)
      .mapAsync(parallelism)(lineParser)

  private val lineDelimiter: Flow[ByteString, ByteString, NotUsed] =
    Framing.delimiter(ByteString("\n"), 128, allowTruncation = true)

  /** Parse clients.txt file lines
    * Example of the line: C1  1000    10  5   15  0
    *
    * @param line file line
    * */
  def parseClientLine(line: String): Future[Option[Client]] = Future {
    val fields = line.split("\t")
    Try {
      Client(
        fields(0),
        fields(1).toInt,
        ListMap(
          Stock.A -> fields(2).toInt,
          Stock.B -> fields(3).toInt,
          Stock.C -> fields(4).toInt,
          Stock.D -> fields(5).toInt
        )
      )
    } recoverWith {
      case NonFatal(t) =>
        logger.error(s"Unable to parse client line:\n$line: ${t.getMessage}")
        Failure(t)
    } toOption
  }

  /** Parse orders.txt file lines
    * Example of the line: C1  b   A   7   12
    *
    * @param line file line
    * */
  def parseOrderLine(line: String): Future[Option[Order]] = Future {
    val fields = line.split("\t")
    Try {
      Order(
        fields(0),
        Operation.getValue(fields(1)),
        Stock.withName(fields(2)),
        fields(3).toInt,
        fields(4).toInt
      )
    } recoverWith {
      case NonFatal(t) =>
        logger.error(s"Unable to parse order line:\n$line: ${t.getMessage}")
        Failure(t)
    } toOption
  }
}
