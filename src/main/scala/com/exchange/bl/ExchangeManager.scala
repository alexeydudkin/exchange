package com.exchange.bl

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.exchange.model.Client
import com.exchange.Messages._
import com.exchange.model.Client.ClientName
import akka.util.Timeout
import com.exchange.bl.service.ClientService

import scala.concurrent.duration._

object ExchangeManager {
  def props(service: ClientService) = Props(classOf[ExchangeManager], service)
}

/** Exchange manager actor
  * Initialize clients
  * Process client orders and store the result
  * @param storeService client service
  * */
class ExchangeManager(storeService: ClientService) extends Actor with ActorLogging {
  implicit val timeout = Timeout(5 seconds)
  implicit val sys = context.system
  implicit val execCtx = context.dispatcher

  override def receive: Receive = initialize()

  private def initialize(clients: Set[Client] = Set.empty): Receive = {
    case GetClients =>
      sender() ! ClientsResponse(clients)

    case ProcessClient(client) =>
      log.debug(s"Initialize client: $client")
      context become initialize(clients + client)

    case ProcessClientsComplete =>
      log.debug(s"Initialize complete with clients: $clients")
      context become
        workWith(
          storeService.storeClientsRef(clients)(context)
        )
  }

  private def workWith(clients: Map[ClientName, ActorRef]): Receive = {
    case msg@ApplyClientOrder(order) =>
      clients.get(order.clientName) foreach (_ ! msg)
    case ProcessOrdersComplete =>
      log.debug("Process orders complete, saving result")
      Exporter().storeClients { ref =>
        clients.values.foreach(clientRef => clientRef ! ExportClientInfo(ref))
        ref ! akka.actor.Status.Success("done")
      }
  }
}