package com

import akka.actor.ActorRef
import com.exchange.model.{Client, Order}

package object exchange {
  object Operation extends Enumeration {
    val Buy, Sell = Value

    def getValue(name: String) = name match {
      case "b" => Buy
      case "s" => Sell
      case  v  => throw new NoSuchElementException(s"No value found for '$v'")
    }
  }

  object Stock extends Enumeration {
    val A, B, C, D = Value
  }

  object Messages {

    case object ProcessClientsComplete

    case object ProcessOrdersComplete

    case object GetClients

    case class ClientsResponse(clients: Set[Client])

    case class ProcessClient(client: Client)

    case class ApplyClientOrder(order: Order)

    case class ExportClientInfo(exporter: ActorRef)

    case object GetClientInfo

    case class ClientInfoResponse(client: Client)

  }
}
