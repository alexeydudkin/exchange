package com.exchange.model

import com.exchange.Stock
import com.exchange.model.Client.ClientName
import scala.collection.immutable.ListMap


/** Client
  * @constructor create new client
  * @param name client name
  * @param balanceUSD client balance in USD
  * @param clientStock info about client stocks in pieces
  * */
case class Client(
                 name: ClientName,
                 balanceUSD: Int,
                 clientStock: ListMap[Stock.Value, Int]){

  def prettyPrint: String =
    (name :: balanceUSD.toString :: Nil ++
    clientStock.values) mkString "\t"
}
object Client {
  type ClientName = String
}

