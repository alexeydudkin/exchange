package com.exchange.model

import com.exchange.model.Client.ClientName
import com.exchange.{Operation, Stock}

/** Order
  * @constructor create new client order
  * @param clientName the name of the client who submitted the order
  * @param operation buy or sell
  * @param stock the name of the stock
  * @param price the order price
  * @param quantity the quantity of stocks sold or purchased
  * */
case class Order(
                  clientName: ClientName,
                  operation: Operation.Value,
                  stock: Stock.Value,
                  price: Int,
                  quantity: Int
                )