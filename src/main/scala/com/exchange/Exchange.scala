package com.exchange

import akka.actor.ActorSystem
import com.exchange.bl.service.{ClientService, ClientServiceImpl}
import com.exchange.bl.{ExchangeManager, Importer}

/** Main runnable application object
  * */
object Exchange extends App {
  implicit val actorSystem = ActorSystem("Exchange")

  lazy val clientService: ClientService = new ClientServiceImpl()

  val clientManager = actorSystem.actorOf(ExchangeManager.props(clientService), "exchange-manager")

  Importer(clientManager).processClients
  Importer(clientManager).processOrders
}
