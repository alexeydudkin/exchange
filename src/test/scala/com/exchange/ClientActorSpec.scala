package com.exchange

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import com.exchange.Messages._
import com.exchange.bl.ClientActor
import com.exchange.model.Client
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ClientActorSpec extends TestKit(ActorSystem("Client-Actor-Spec"))
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll
  with ImplicitSender
  with TestData {

  "Client actor" must {
    "process buy operation" in {
      val clientActor = TestActorRef[ClientActor](ClientActor.props(Client1))

      def checkClientInfo: Client => Boolean = client =>
        client.balanceUSD == 80 && client.clientStock(Order1.stock) == 12

      clientActor ! ApplyClientOrder(Order1)
      clientActor ! GetClientInfo
      expectMsgPF() {
        case ClientInfoResponse(client) if checkClientInfo(client)  =>
      }
    }

    "process sell operation" in {
      val clientActor = TestActorRef[ClientActor](ClientActor.props(Client3))

      def checkClientInfo: Client => Boolean = client =>
        client.balanceUSD == 550 && client.clientStock(Order2.stock) == 5

      clientActor ! ApplyClientOrder(Order2)
      clientActor ! GetClientInfo
      expectMsgPF() {
        case ClientInfoResponse(client) if checkClientInfo(client)  =>
      }
    }

    "skip order if not enough balance to buy" in {
      val clientActor = TestActorRef[ClientActor](ClientActor.props(Client2))

      clientActor ! ApplyClientOrder(Order3)
      clientActor ! GetClientInfo
      expectMsgPF() {
        case ClientInfoResponse(client) if client.equals(Client2)  =>
      }
    }

    "skip order if not enough stocks to sell" in {
      val clientActor = TestActorRef[ClientActor](ClientActor.props(Client1))

      clientActor ! ApplyClientOrder(Order4)
      clientActor ! GetClientInfo
      expectMsgPF() {
        case ClientInfoResponse(client) if client.equals(Client1)  =>
      }
    }
  }

  override protected def afterAll() {
    TestKit.shutdownActorSystem(system)
  }
}