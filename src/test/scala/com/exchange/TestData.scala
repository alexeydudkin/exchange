package com.exchange

import com.exchange.model.{Client, Order}

import scala.collection.immutable.ListMap

trait TestData {
  val Client1 = Client("C1", 100, ListMap(Stock.A -> 10, Stock.B -> 10, Stock.C -> 10, Stock.D -> 10))
  val Client2 = Client("C2", 200, ListMap(Stock.A -> 20, Stock.B -> 20, Stock.C -> 20, Stock.D -> 20))
  val Client3 = Client("C3", 300, ListMap(Stock.A -> 30, Stock.B -> 30, Stock.C -> 30, Stock.D -> 30))

  val Order1 = Order(Client1.name, Operation.Buy, Stock.A, 10, 2)
  val Order2 = Order(Client3.name, Operation.Sell, Stock.D, 10, 25)
  val Order3 = Order(Client2.name, Operation.Buy, Stock.D, 10, 25)
  val Order4 = Order(Client1.name, Operation.Sell, Stock.B, 5, 25)
}
