package com.exchange

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.testkit.{TestKit, TestProbe}
import com.exchange.bl.Exporter
import com.exchange.model.Client
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.duration._
import scala.concurrent.Await

class ExporterSpec extends TestKit(ActorSystem("Exporter-Spec"))
  with WordSpecLike
  with Matchers
  with TestData {

  implicit val materializer = ActorMaterializer()

  val clientManager = new TestProbe(system)
  val exporter = Exporter()

  "Exporter actor" must {
    "process resulting client info " in {
      val clientResult =
        """|C1	100	10	10	10	10
          |C2	200	20	20	20	20
          |C3	300	30	30	30	30
          |""".stripMargin

      val (ref, future) =
        Source.actorRef[Client](8, OverflowStrategy.dropHead)
        .toMat(exporter.exportFlow.toMat(Sink.fold("")(_ + _))(Keep.right))(Keep.both).run()

      ref ! Client1
      ref ! Client2
      ref ! Client3
      ref ! akka.actor.Status.Success("done")

      val result = Await.result(future, 5 seconds)
      assert(result == clientResult)

    }
  }
}
