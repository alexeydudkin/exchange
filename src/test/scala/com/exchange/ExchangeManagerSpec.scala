package com.exchange

import akka.actor.{ActorContext, ActorSystem}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import com.exchange.Messages._
import com.exchange.bl.service.{ClientService, ClientServiceImpl}
import com.exchange.bl.ExchangeManager
import com.exchange.model.Client
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ExchangeManagerSpec extends TestKit(ActorSystem("Exchange-Manager-Spec"))
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll
  with ImplicitSender
  with MockFactory
  with TestData {

  "Exchange manager" must {
    "process clients" in {
      val manager = TestActorRef[ExchangeManager](ExchangeManager.props(new ClientServiceImpl()))
      manager ! ProcessClient(Client1)
      manager ! ProcessClient(Client2)
      manager ! GetClients
      expectMsgPF() {
        case ClientsResponse(clients) if clients.equals(Set(Client1, Client2)) =>
      }
    }

    "process order to client" in {
      val probe = new TestProbe(system, "C1")
      val service = mock[ClientService]
      val manager = TestActorRef[ExchangeManager](ExchangeManager.props(service))

      (service.storeClientsRef(_: Set[Client])(_: ActorContext)).expects(*, *).returning(Map("C1" -> probe.ref))

      manager ! ProcessClient(Client1)
      manager ! ProcessClientsComplete
      manager ! ApplyClientOrder(Order1)

      probe.expectMsgPF() {
        case ApplyClientOrder(order) if order.equals(Order1) =>
      }
    }
  }

  override protected def afterAll() {
    TestKit.shutdownActorSystem(system)
  }
}