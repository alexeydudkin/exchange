package com.exchange

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.FileIO
import akka.stream.testkit.scaladsl.TestSink
import akka.testkit.{TestKit, TestProbe}
import com.exchange.bl.Importer
import com.exchange.model.{Client, Order}
import org.scalatest.{Matchers, WordSpecLike}

class ImporterSpec extends TestKit(ActorSystem("Importer-Spec"))
  with WordSpecLike
  with Matchers
  with TestData {

  implicit val materializer = ActorMaterializer()

  val clientManager = new TestProbe(system)
  val importer = Importer(clientManager.ref)

  "Importer actor" must {
    "read clients" in {
      val flow =
        FileIO.fromPath(Paths.get(getClass.getResource("/clients-test.txt").toURI))
          .via(importer.fileProcessFlow(4)(importer.parseClientLine))

      flow.runWith(TestSink.probe[Option[Client]])
        .request(5)
        .expectNextUnordered(Some(Client1), Some(Client2), Some(Client3))
        .expectComplete()
    }

    "read orders" in {
      val flow =
        FileIO.fromPath(Paths.get(getClass.getResource("/orders-test.txt").toURI))
          .via(importer.fileProcessFlow(1)(importer.parseOrderLine))

      flow.runWith(TestSink.probe[Option[Order]])
        .request(5)
        .expectNextUnordered(Some(Order1), Some(Order2))
        .expectComplete()
    }
  }
}