import sbt.Build
import sbt.Keys._
import sbt._

object ExchangeAppBuild extends Build {
  val buildSettings = Seq(
    name := """Exchange""",
    version := "1.0",
    scalaVersion := "2.11.6",
    libraryDependencies ++= appDependencies,
    resolvers ++= appResolvers
  )

  val appResolvers = Seq(
    Resolver.sonatypeRepo("releases")
  )

  val appDependencies = Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.3.11",
    "com.typesafe.akka" %% "akka-testkit" % "2.3.11" % Test,
    "org.scalatest" % "scalatest_2.11" % "3.0.1" % Test,
    "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test,
    "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0",
    "ch.qos.logback" % "logback-classic" % "1.1.7",
    "com.typesafe.akka" % "akka-slf4j_2.11" % "2.4.8",
    "com.typesafe.akka" %% "akka-stream" % "2.4.7",
    "com.typesafe.akka" %% "akka-stream-testkit" % "2.4.7" % Test

  )

  import com.github.retronym.SbtOneJar._

  lazy val app = (project in file(".")).settings(buildSettings ++ oneJarSettings ++ Seq(
    mainClass in oneJar := Some("com.exchange.Exchange"),
    artifactName in oneJar := {(sv: ScalaVersion, module: ModuleID, artifact: Artifact) ⇒
      "exchange.jar"
    }) :_*)

}